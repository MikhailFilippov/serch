﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace SearcFile
{
    public partial class SearchForm : Form
    {
        public SearchForm()
        {
            InitializeComponent();
        }

        private void btnPath_Click(object sender, EventArgs e) //выбрать путь
        {
            if (fBrowserDialog_Path.ShowDialog() == DialogResult.OK)
            {
                txtBoxStartDirectory.Text = fBrowserDialog_Path.SelectedPath;
            }
        }
        Stopwatch sw;
        DirectoryInfo dirTest = null;
        DirectoryInfo directoryInfo;
        Thread thread;
        private void btnSearch_Click(object sender, EventArgs e) //поиск
        {
            try
            {
                label8.Visible = true;
                if (txtBoxStartDirectory.Text == "" || txtBoxTemplateNameFile.Text == "" || txtBoxTextFile.Text == "")
                {
                    MessageBox.Show("Все поля должны быть заполнены!");
                }
                else
                {
                    number = 0;
                    count = 0;
                    treeViewSearch.Nodes.Clear();
                    txtBoxProcessingFile.Text = "";
                    txtBoxNumberFile.Text = "";
                    txtBoxTime.Text = "";
                    directoryInfo = new DirectoryInfo(txtBoxStartDirectory.Text);
                    treeViewSearch.BeginUpdate();
                    sw = new Stopwatch();
                    sw.Start();
                    int nod = Search(directoryInfo, treeViewSearch.Nodes);

                    bool b = false;
                    thread = new Thread(() => ProcessingFile(directoryInfo));
                    thread.Start();
                    sw.Stop();
                    if (nod == 0)
                    {

                        thread.Join();
                        txtBoxTime.Text = (sw.ElapsedMilliseconds / 100.0).ToString();

                        MessageBox.Show("Объект не найден!!");
                        label8.Visible = false;
                        treeViewSearch.Nodes.Clear();
                        txtBoxTime.Text = "";
                        txtBoxNumberFile.Text = "";
                        txtBoxProcessingFile.Text = "";
                        thread.Abort();

                    }
                    else
                    {
                        txtBoxTime.Text = (sw.ElapsedMilliseconds / 100.0).ToString();
                        label8.Visible = false;
                    }

                    treeViewSearch.EndUpdate();
                }
            }
            catch (Exception)
            {

            }     
        }
        static int number = 0;
        bool bol = false;
        private void ProcessingFile(DirectoryInfo directory)
        {
            try
            {
                foreach (FileInfo file in directory.GetFiles()) //список файлов в каталоге
                {


                    txtBoxProcessingFile.Text = file.Name;
                    number++;
                    txtBoxNumberFile.Text = number.ToString();


                }

                foreach (DirectoryInfo dir in directory.GetDirectories()) //поиск в подпапках
                {
                    ProcessingFile(dir);
                }

            }
            catch (Exception ex)
            {

            }
            bol = true;
        }
        int count = 0;
        private int Search(DirectoryInfo dir, TreeNodeCollection treeNodeCollection)
        {
            

            TreeNode curNode = treeNodeCollection.Add("Folder", dir.Name); //добавляем узел
            string readFile;
            try
            {
                foreach (DirectoryInfo subdir in dir.GetDirectories()) //поиск в подпапках
                {
                    Search(subdir, curNode.Nodes);
                }

                foreach (FileInfo file in dir.GetFiles()) //список файлов в каталоге
                {
                    if (file.Name.EndsWith(txtBoxTemplateNameFile.Text)) //если шаблон имени файла совпадает с файлом в папке
                    {
                        using (StreamReader streamReader = new StreamReader(file.FullName, System.Text.Encoding.Default))
                        {
                            while ((readFile = streamReader.ReadLine()) != null) //считываем построчно
                            {
                                if (readFile.IndexOf(txtBoxTextFile.Text) > 0) // проверяем, есть ли такое слово в файле
                                {
                                    count++;
                                    //Добавляем в treeView
                                    if (dirTest != dir)
                                    {
                                        curNode.Nodes.Add("File", file.Name);
                                        dirTest = dir;
                                    }
                                    else
                                    {
                                        curNode.Nodes.Add("File", file.Name);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //sw.Reset();
                MessageBox.Show("Ошибка поиска файлов! " + ex.Message);
                if (thread != null && thread.IsAlive)
                {
                    thread.Abort();
                }

                return 0;
            }

            return count;
        }

        private void SearchForm_Load(object sender, EventArgs e)
        {
            // передаем в конструктор тип класса
            XmlSerializer xml = new XmlSerializer(typeof(SaveData));
            //Десериализация
            FileStream fs = new FileStream("save.xml", FileMode.OpenOrCreate);
            if (fs.Length != 0)
            {
                SaveData data = (SaveData)xml.Deserialize(fs);
                txtBoxStartDirectory.Text = data.StartDirectory;
                txtBoxTemplateNameFile.Text = data.TemplateNameFile;
                txtBoxTextFile.Text = data.TextFile;
            }
            fs.Close();
        }

        private void SearchForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Объект для сериализации
            SaveData saveData = new SaveData(txtBoxStartDirectory.Text, txtBoxTemplateNameFile.Text, txtBoxTextFile.Text);

            XmlSerializer xml = new XmlSerializer(typeof(SaveData));

            //запись
            FileStream fs = new FileStream("save.xml", FileMode.Create);
            xml.Serialize(fs, saveData);
            fs.Close();

        }

        private void btnResume_Click(object sender, EventArgs e)
        {
            backgroundWorkerSearch.WorkerSupportsCancellation = true;
            backgroundWorkerSearch.RunWorkerAsync(); //запускаем поток
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            if (thread != null && thread.IsAlive)
            {
                thread.Suspend();
            }
        }

        private void backgroundWorkerSearch_DoWork(object sender, DoWorkEventArgs e)
        {
            thread.Resume();
        }
    }
}
